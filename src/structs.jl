"""
	SteamController
Used to represent a Steam Controller.
"""
mutable struct SteamController
	id::String
	event_handle::String
end

# Pretty-printing for Controller's
function Base.show(io::IO, controller::Controller)
	print(io, "Steam Controller $(controller.id) [$(controller.event_handle)]")
end

"""
	EvdevEvent{T}
Data from a Linux evdev packet. The size of these packets varies by system
architecture. Generally speaking, use type Int64 for 64-bit Linux and Int32
for 32-bit Linux.
"""
struct EvdevEvent{T}
	tm_sec::T
	tm_usec::T
	type::UInt16
	code::UInt16
	value::UInt32
end

# Pretty-printing for EvdevEvent's
function Base.show(io::IO, event::EvdevEvent)
	tm_str = "$(event.tm_sec).$(lpad(event.tm_usec,6,"0"))"
	print(io, "Event at $(tm_str): type $(event.type), code $(event.code), value $(event.value)")
end

