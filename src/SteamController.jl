module SteamController

	include("structs.jl")
	export SteamController

# /usr/include/linux/input-event-codes.h
eventtype = Dict{UInt8,Symbol}( 0x00 => :EV_SYN,       # "markers to separate events"
			0x01 => :EV_KEY,       # state change of key-like devices
			0x02 => :EV_REL,       # relative axis value change
			0x03 => :EV_ABS,       # absolute axis value change
			0x04 => :EV_MSC,       # miscellaneous input data
			0x05 => :EV_SW,        # binary state input switches
			0x11 => :EV_LED,       # turn LED's on/off
			0x12 => :EV_SND,       # output sound to device
			0x14 => :EV_REP,       # used for autorepeating devices
			0x15 => :EV_FF,        # used to send force feedback commands
			0x16 => :EV_PWR,       # for power button/switch input
			0x17 => :EV_FF_STATUS, # force feedback device status
			0x1f => :EV_MAX,       # 
			0x20 => :EV_CNT
			)

buttonmap = Dict{UInt16,Symbol}( 0x0000 => :Analog_Y,
				 0x0001 => :Analog_X,
				 0x0010 => :LTouch_X,
				 0x0011 => :LTouch_Y,
				 0x0014 => :RTrigger,
				 0x0015 => :LTrigger,
				 0x0121 => :LTouch_Touching,
				 0x0122 => :RTouch_Touching,
				 0x0130 => :A,
				 0x0131 => :B,
				 0x0133 => :X,
				 0x0134 => :Y,
				 0x0136 => :LBumper,
				 0x0137 => :RBumper,
				 0x0138 => :LTrigger_Click,
				 0x0139 => :RTrigger_Click,
				 0x013a => :Select,
				 0x013b => :Start,
				 0x013c => :SteamButton,
				 0x013e => :RTouch_Click
				 0x0150 => :LGrip,
				 0x0151 => :RGrip,
				 0x0220 => :LTouch_UClick,
				 0x0221 => :LTouch_DClick,
				 0x0223 => :LTouch_LClick,
				 0x0224 => :LTouch_RClick      )

function getcontrollerhandle(n=1)
	regex_event_joystick = r"usb-Valve_Software_Steam_Controller-(.+)-event-joystick"

	# Get list of all input devices by-id, find a Steam Controller event-joystick
	devices = readdir("/dev/input/by-id")
	filter!(x->!isnothing(match(regex_event_joystick,x)), devices)
	devices = map(x->joinpath("/dev/input/by-id", x), devices)
	
	# Return the n'th controller, if present
	if ( n <= length(devices) )
		return devices[n]
	else
		return missing
	end
end

function getevent(handle::IOStream, T::DataType)
	evbuffer = zeros(UInt8, sizeof(T))
	readbytes!(handle, evbuffer, sizeof(T))
	event::T = reinterpret(T, evbuffer)[1]
	return event
end

function monitorevents(fname)
	evhandle = open(fname, "r")
	while true
		try
			event = getevent(evhandle, evdevEvent{Int64})
			println(event)
		catch e
			println(e)
			break
		end
	end
end

end
