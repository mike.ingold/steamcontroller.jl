# SteamController.jl

*Status:* Does not yet compile. Effort was temporarily diverted to the C version for reasons stated below.

## Julia Support for ARM

As of 2021, Julia does not support 32-bit ARMv6 at all and only supports 32-bit ARMv7 at Tier 3: "Julia may or may not build. If it does, it is unlikely to pass tests. Binaries may be available in some cases. When they are, they should be considered experimental. Ongoing support is dependent on community efforts."<sup>[1]</sup>

Raspberry Pi 3- and 4-series boards are 64-bit ARMv8, which Julia currently supports at Tier 2: "Julia is guaranteed to build from source using the default build options, but may or may not pass all tests. Official binaries are available on a case-by-case basis."<sup>[1]</sup>

Work is ongoing in the Julia community to increase ARM compatibility, largely due to the introduction of the M1 Mac, so this situation may improve over time. However, ARMv6 support is probably never going to happen and 32-bit ARMv7 support will likely lose support in future versions.

Another version of this library was written in C to more reliably support the ARMv6-class Raspberry Pi models.

[1] https://julialang.org/downloads/
